package edu.nwmissouri.randomgroupgenerator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import listviewoperations.Group;

/**
 * Created by sandi on 12/6/2015.
 */
public class BaseActivity extends AppCompatActivity {


    protected ArrayList<Group> groups =new ArrayList<>();
   // protected ArrayList<Group> groups=new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public ArrayList<Group> getGroups() {
        return groups;
    }

    public void setGroups(ArrayList<Group> groups) {
        this.groups = groups;
    }
}
