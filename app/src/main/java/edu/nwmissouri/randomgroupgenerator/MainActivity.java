package edu.nwmissouri.randomgroupgenerator;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.TypefaceProvider;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import listviewoperations.Group;

public class MainActivity extends BaseActivity implements ShowGroupsFragment.OnFragmentInteractionListener {

    static boolean fileLoadingFirstTime = false;
    static File selectedFile;
    public static ArrayList<Group> groupArrayList;
    public static String[] nameArray;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    public static int sizeOfGroup = 0;
    private ViewPager mViewPager;
    public static String className;

    public static ArrayList<Group> groupList;
    private int timesSaveBeenHit;

    public  static int totalNumberOfSave = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // doing for the bootstrap
        TypefaceProvider.registerDefaultIconSets();

        Parse.enableLocalDatastore(this);

        Parse.initialize(this, "lg8niupYUW7KbV6yskx7fJfiB3gqynBxK6ZgAZXM", "LK9DmyxIFpUIjbjoGmAcey942pNvg5MSKeUFwcco");
        populateNewDataToGroupArrayList();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());


        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }


    @Override
    public ArrayList<Group> getGroups() {
        return super.getGroups();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_group_picker, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    public void saveGroup(View v) {
        if(timesSaveBeenHit==0){
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            // groupList.get(0).getProfileBitmap().compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();

            for (int i = 0; i < groupList.size(); i++) {
                ParseObject groupsData1 = new ParseObject("GroupsData");
                // groupsData1.addAll("groupItem", temp);
                stream = new ByteArrayOutputStream();
                if (groupList.get(i).getProfileBitmap() != null) {
                    groupList.get(i).getProfileBitmap().compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byteArray = stream.toByteArray();
                    ParseFile pf = new ParseFile("image", byteArray);
                    groupsData1.put("Image", pf);
                }
                groupsData1.put("groupItem1", groupList.get(i).getGroupName());
                groupsData1.put("groupNames", groupList.get(i).getNames());

                groupsData1.saveInBackground(new SaveCallback() {
                    public void done(ParseException e) {
                        if (e == null) {
                        } else {
                        }
                    }
                });

            }
            Toast.makeText(getApplicationContext(),"Data saved!!!",Toast.LENGTH_SHORT).show();
            groupArrayList.addAll(groupList);
        }
        else{
            Toast.makeText(getApplicationContext(),"Data have been saved already.", Toast.LENGTH_SHORT).show();
        }
        timesSaveBeenHit = 1;
        totalNumberOfSave = 1;
    }

    static void populateNewDataToGroupArrayList() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("GroupsData");
        groupArrayList = new ArrayList<>();

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> groupsList1, ParseException e) {
                if (e == null) {
                    int count = 0;
                    for (ParseObject object : groupsList1) {
                        Group group = new Group(object.getString("groupItem1"), new String[]{"Adf", "adf"});
                        group.setNames(object.getString("groupNames"));

                        ParseFile bum = (ParseFile) object.get("Image");
                        byte[] file = new byte[0];
                        try {
                            if (bum != null) {
                                file = bum.getData();
                                group.setProfileBitmap(BitmapFactory.decodeByteArray(file, 0, file.length));
                            }
                        } catch (ParseException e1) {
                            e1.printStackTrace();
                        }

                        groupArrayList.add(group);
                        count++;
                    }

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return new GroupGeneratorFragment();
                case 1:
                    return new ShowGroupsFragment();
                case 2:
                    Log.d("APPLE", "Called in main");
                    return new GroupsHistoryFragment();
            }
            return null;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            // Show 2 fragments in total
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Details";
                case 1:
                    return "Result";
                case 2:
                    return "History";
            }
            return null;
        }
    }

    public void browseDirectory(View v) {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        //Show the file chooser
        showFileChooser();
    }


//    private void showConfirmation(String[] string) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//        builder.setTitle("Are you sure you want to proceed with the list below?");
//        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, string);
//        builder.setAdapter(stringArrayAdapter, null);
//        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(getApplicationContext(), "Hit Generate button now", Toast.LENGTH_LONG).show();
//            }
//        });
//        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//
//        builder.create().show();
//
//    }

    /*
    File Chooser operation starts below
     */
    private static final int FILE_SELECT_CODE = 0;

    private void showFileChooser() {
        //initializing the file with null
        MainActivity.selectedFile = null;

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    //Log.d(getClass().getSimpleName(), "File Uri: " + uri.toString());
                    // Get the path
                    String path = null;
                    try {
                        path = getPath(this, uri);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    // Log.d(getClass().getSimpleName(), "File Path: " + path);

                    MainActivity.selectedFile = new File(path);
                    // if(resultCode == 0){
                    EditText fnET = (EditText) findViewById(R.id.fileNameET);
                    fnET.setText(path);
                    // }

                }
                break;
            case 199:
                Bitmap data1 = (Bitmap) data.getExtras().get("data");


                Iterator<Group> iterator = getGroups().iterator();
                Group newObject = null;
                for (int i = 0; i <= Constants.POSITION; i++) {

                    newObject = iterator.next();
                }
                newObject.setProfileBitmap(data1);


                performClick();

                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public String getPath(Context context, Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    /*
    Once all operation is completed and user is satisfied with the data they entered
    hitting generate would take to show groups dialog
     */
    public void Generate(View v) {
        timesSaveBeenHit =0;
        //Take input from the edit text
        EditText fET = (EditText) findViewById(R.id.fileNameET);
        String input = fET.getText().toString().trim();


        EditText className = (EditText) findViewById(R.id.className);

        //Checking if file exists
        if (input.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Enter a file name first", Toast.LENGTH_LONG).show();
        }else if (className.getText().toString().length() == 0) {
            Toast.makeText(getApplicationContext(), "Please enter the class name", Toast.LENGTH_LONG).show();
        } else {
            MainActivity.className = className.getText().toString();
            if (MainActivity.selectedFile != null) {
                getDetails();
            } else {
                if (checkIfFileExist(input)) {
                    getDetails();
                }
            }
        }
    }

    //In case something is entered in the edit text make sure things are alright
    private void getDetails() {
        EditText gET = (EditText) findViewById(R.id.groupSizeET);
        String numberString = gET.getText().toString();

        //Set size of group  to zero if nothing is added here.
        if (numberString.length() == 0) {
            sizeOfGroup = 2;
        } else {
            sizeOfGroup = numberString.length() == 0 ? 2 : Integer.parseInt(numberString);
        }
        ShowGroupsFragment.groups.clear();
        MainActivity.fileLoadingFirstTime = true;
        performClick();
    }


    private boolean checkIfFileExist(String s) {
        File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + s);
        if (!f.exists()) {
            Toast.makeText(getApplicationContext(), "File could not be found.\nMake sure your file is in root folder", Toast.LENGTH_LONG).show();
            return false;
        } else {
            MainActivity.selectedFile = f;
            return true;
        }
    }

    private void performClick() {
        mViewPager.setCurrentItem(1);
        mViewPager.getAdapter().notifyDataSetChanged();
    }
}
