package edu.nwmissouri.randomgroupgenerator;


import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.TypefaceProvider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import listviewoperations.CustomArrayAdapter;
import listviewoperations.Group;



public class
ShowGroupsFragment extends Fragment {


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private ListView listView;



    private OnFragmentInteractionListener mListener;
    public static ArrayList<Group> groups;


    public ShowGroupsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        // for bootstrap
        TypefaceProvider.registerDefaultIconSets();

        groups = ((MainActivity) getActivity()).getGroups();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_show_groups, container, false);

        listView = (ListView) view.findViewById(R.id.list_view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateData();
    }

    public void updateData() {
        if(MainActivity.fileLoadingFirstTime){
            ArrayList<String> tempArray = new ArrayList<>();
            if (MainActivity.selectedFile != null || MainActivity.nameArray != null) {
                if (MainActivity.selectedFile == null && MainActivity.nameArray != null) {
                    tempArray.addAll(Arrays.asList(MainActivity.nameArray));
                } else {
                    try {
                        Scanner ourScanner = new Scanner(MainActivity.selectedFile);
                        while (ourScanner.hasNextLine()) {
                            //Log.d("inside the while loop", "we are inside");
                            tempArray.add(ourScanner.nextLine());
                        }
                    } catch (Exception e) {
                        Toast.makeText(getContext(), "Could not access the file", Toast.LENGTH_LONG).show();
                    }
                }

                //Carry the shuffling and creating listview
                //   Collections.shuffle(tempArray);
                //If groupSize is left to zero By default we are making group size to 2;
                int groupSize = MainActivity.sizeOfGroup == 0 ? 2 : MainActivity.sizeOfGroup;
                int times = tempArray.size() / groupSize;
                int currentIndex = 0;
                for (int i = 0; i < times; i++) {
                    String[] names = new String[groupSize];
                    for (int j = 0; j < groupSize; j++) {
                        names[j] = tempArray.get(currentIndex);
                        currentIndex++;
                    }
                    groups.add(new Group(MainActivity.className+"_Group " + (i + 1), names));
                }
                //Last group with remaining students
                int index = 0;
                int remainingMembers = tempArray.size() % groupSize;
                if (remainingMembers != 0) {
                    String[] names = new String[tempArray.size() - currentIndex];
                    for (int k = currentIndex; k < tempArray.size(); k++) {
                        names[index] = tempArray.get(k);
                        currentIndex++;
                        index++;
                    }
                    groups.add(new Group(MainActivity.className+"_Group " + (times + 1), names));
                }
            }

            MainActivity.fileLoadingFirstTime = false;
        }

        CustomArrayAdapter customArrayAdapter = new CustomArrayAdapter(getActivity(), R.layout.custom_list_item, R.id.gpNameET, groups);
        MainActivity.groupList =  new ArrayList<>(groups);
        listView.setOverScrollMode(View.OVER_SCROLL_IF_CONTENT_SCROLLS);
        listView.setAdapter(customArrayAdapter);
        customArrayAdapter.notifyDataSetChanged();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
