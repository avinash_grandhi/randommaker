package edu.nwmissouri.randomgroupgenerator;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;

import listviewoperations.Group;
import listviewoperations.HistoryCustomArrayAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class GroupsHistoryFragment extends Fragment {


    private ExpandableListView expListView;
    HistoryCustomArrayAdapter listAdapter;
    List<String> listDataHeader;
    HashMap<String, List<Group>> listDataChild;
    private boolean searchInProgress = false;
    private ArrayList<String> temporarySearched;

    public GroupsHistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_groups_history, container, false);
        expListView = (ExpandableListView) view.findViewById(R.id.lvExp);

        expListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                int itemType = ExpandableListView.getPackedPositionType(id);
                int childPosition =0 ;
                int groupPosition= 0;
                if (itemType == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
                    childPosition = ExpandableListView.getPackedPositionChild(id);
                    groupPosition = ExpandableListView.getPackedPositionGroup(id);
                    Toast.makeText(getContext(),
                            " Expanded  "+listDataHeader.get(groupPosition),
                            Toast.LENGTH_SHORT).show();
                    showDialogForDelete(listDataHeader.get(groupPosition));
                    //do your per-item callback here
                    return true; //true if we consumed the click, false if not

                } else{
                    return false;
                }


            }
       });
        prepareListData();
        updateData();


        EditText inputSearch = (EditText) view.findViewById(R.id.inputSearch);
        View.OnFocusChangeListener ofcListener = new MyFocusChangeListener();
        inputSearch.setOnFocusChangeListener(ofcListener);
        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                Iterator i = listDataHeader.iterator();
                temporarySearched = new ArrayList<>();
                while (i.hasNext()) {
                    String className = (String) i.next();
                    if (!className.toLowerCase().contains(cs.toString().toLowerCase())) {
                        temporarySearched.add(className);
                    }
                }
                searchInProgress = true;
                onResume();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;

    }

    private class MyFocusChangeListener implements View.OnFocusChangeListener {

        public void onFocusChange(View v, boolean hasFocus) {

            if (v.getId() == R.id.inputSearch && !hasFocus) {

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            }
        }
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<Group>>();

        ArrayList<String> groupNames = new ArrayList<>();
        ArrayList<ArrayList<Group>> arGroups = new ArrayList<>();
        int index = -1;
        for (Group currentGroup : MainActivity.groupArrayList) {
            String key = currentGroup.getGroupName().split("_")[0];
            if (groupNames.contains(key)) {
                arGroups.get(index).add(currentGroup);
            } else {
                index++;
                groupNames.add(key);
                arGroups.add(new ArrayList<Group>());
                arGroups.get(index).add(currentGroup);
            }
        }

        //Resetting index to read from arrayList of names
        index = 0;
        for (ArrayList<Group> temp : arGroups) {
            listDataChild.put(groupNames.get(index), temp);
            index++;
        }
        listDataHeader.addAll(groupNames);
    }

    private void showDialogForDelete(final String name) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Are you sure you want delete the class?");
//                    ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, string);
//        builder.setAdapter(stringArrayAdapter, null);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ParseQuery<ParseObject> query = ParseQuery.getQuery("GroupsData");
                query.whereStartsWith("groupItem1", name + "_Group");
                query.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> groupList, ParseException e) {
                        if (e == null) {
                            for (ParseObject o : groupList) {
                                o.deleteInBackground();
                            }
                            Toast.makeText(getContext(), "Class item " + name + " is deleted successfully.",
                                    Toast.LENGTH_SHORT).show();
                            MainActivity.populateNewDataToGroupArrayList();
                            onResume();
                        } else {}
                    }
                });
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.create().show();
    }


    @Override
    public void onResume() {
        super.onResume();
        updateData();
    }

    public void updateData() {
        List<String> updatedHeader = new ArrayList<>(listDataHeader);
        HashMap<String, List<Group>> updatedChildData = new HashMap<>(listDataChild);
        if (temporarySearched != null) {
//            updatedList.removeAll(temporarySearched);
            for (String header : temporarySearched) {
                updatedHeader.remove(header);
                updatedChildData.remove(header);
            }

        }
        listAdapter = new HistoryCustomArrayAdapter(getActivity(), updatedHeader, updatedChildData);
        listAdapter.notifyDataSetChanged();
        expListView.setAdapter(listAdapter);
    }

}
