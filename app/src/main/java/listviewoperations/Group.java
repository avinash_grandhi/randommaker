package listviewoperations;

import android.graphics.Bitmap;

/**
 * Created by sandi on 12/5/2015.
 */
public class Group implements Comparable<Group>{

    String groupName = "";
    String names = "";
    int count = 0;


    Bitmap profileBitmap;

    public Bitmap getProfileBitmap() {
        return profileBitmap;
    }

    public void setProfileBitmap(Bitmap profileBitmap) {
        this.profileBitmap = profileBitmap;
    }

    public Group(String groupName, String[] names) {
        this.groupName = groupName;
        for (int i = 0; i < names.length; i++) {
            this.names += names[i] + "\n";
        }
        this.count = names.length;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object object) {
        if (object != null && object instanceof Group) {
            if (((Group) (object)).names == this.names && ((Group) (object)).groupName == this.groupName)
                return true;
            else return false;
        }return false;
    }

    @Override
    public int compareTo(Group another) {
        return this.getGroupName().compareTo(another.getGroupName());
    }
}
