package listviewoperations;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import edu.nwmissouri.randomgroupgenerator.R;

/**
 * Created by S521697 on 12/8/2015.
 */
public class HistoryCustomArrayAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<Group>> _listDataChild;

    public HistoryCustomArrayAdapter(Context context, List<String> listDataHeader,
                                     HashMap<String, List<Group>> listChildData) {
        this.context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final Group item = (Group)getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.custom_list_item, null);
        }

        TextView gpName = (TextView) convertView.findViewById(R.id.gpNameET);
        TextView groupMembersTV = (TextView) convertView.findViewById(R.id.gpMembersTV);
        ImageView gpImageIV = (ImageView) convertView.findViewById(R.id.gpImageIV);
        TextView countTV = (TextView)convertView.findViewById(R.id.countTV);

        gpName.setText(item.getGroupName());
        groupMembersTV.setText(item.getNames());
        if (item.getProfileBitmap()==null){
            gpImageIV.setImageResource(R.drawable.no_image);
        }else {
            gpImageIV.setImageBitmap(item.getProfileBitmap());
        }
        countTV.setText("Group size: "+item.count);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        //lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}


//    public HistoryCustomArrayAdapter(Context context, int resource, int textViewResourceId, List<Group> groups) {
//        super(context, resource, textViewResourceId, groups);
//        this.context=context;
//    }
//
//    public View getView(final int position, View oldView, ViewGroup parent) {
//        View view = super.getView(position, oldView, parent);
//
//        TextView gpName = (TextView) view.findViewById(R.id.gpNameET);
//        TextView groupMembersTV = (TextView) view.findViewById(R.id.gpMembersTV);
//        ImageView gpImageIV = (ImageView) view.findViewById(R.id.gpImageIV);
//        TextView countTV = (TextView)view.findViewById(R.id.countTV);
//
//
//        Group item = getItem(position);
//
//        gpName.setText(item.getGroupName());
//        groupMembersTV.setText(item.getNames());
//        if (item.getProfileBitmap()==null){
//
//            gpImageIV.setImageResource(R.drawable.camera_image);
//        }else {
//            gpImageIV.setImageBitmap(item.getProfileBitmap());
//        }
//        countTV.setText("Group size: "+getItem(position).count);
//        return view;
//    }
