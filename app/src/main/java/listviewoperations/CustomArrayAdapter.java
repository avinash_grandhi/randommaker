package listviewoperations;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import edu.nwmissouri.randomgroupgenerator.Constants;
import edu.nwmissouri.randomgroupgenerator.R;

/**
 * Created by sandi on 12/5/2015.
 */
public class CustomArrayAdapter extends ArrayAdapter<Group> {
    private Context context;
    public CustomArrayAdapter(Context context, int resource, int textViewResourceId, List<Group> groups) {
        super(context, resource, textViewResourceId, groups);
        this.context=context;
    }

    public View getView(final int position, View oldView, ViewGroup parent) {
        View view = super.getView(position, oldView, parent);

        TextView gpName = (TextView) view.findViewById(R.id.gpNameET);
        TextView groupMembersTV = (TextView) view.findViewById(R.id.gpMembersTV);
        ImageView gpImageIV = (ImageView) view.findViewById(R.id.gpImageIV);
        TextView countTV = (TextView)view.findViewById(R.id.countTV);


        Group item = getItem(position);

        gpName.setText(item.getGroupName());
        groupMembersTV.setText(item.getNames());
//        try {
        //    url = new URL("http://www.gravatar.com/avatar/7ccaf0f60ca8b4a8cb5bb52fafb473f8");

        //Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


        if (item.getProfileBitmap()==null){

        gpImageIV.setImageResource(R.drawable.camera_image);
        }else {
            gpImageIV.setImageBitmap(item.getProfileBitmap());
        }



        gpImageIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Constants.POSITION=position;
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                    ((Activity) context).startActivityForResult(takePictureIntent, 199);
                }
            }
        });
        countTV.setText("Group size: "+getItem(position).getCount());
        return view;
    }
}
